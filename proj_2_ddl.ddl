-- Generated by Oracle SQL Developer Data Modeler 4.1.3.901
--   at:        2016-10-12 18:33:17 CDT
--   site:      Oracle Database 11g
--   type:      Oracle Database 11g




DROP TABLE department CASCADE CONSTRAINTS ;

DROP TABLE employee CASCADE CONSTRAINTS ;

DROP TABLE je CASCADE CONSTRAINTS ;

DROP TABLE job CASCADE CONSTRAINTS ;

DROP TABLE meeting CASCADE CONSTRAINTS ;

DROP TABLE pem CASCADE CONSTRAINTS ;

DROP TABLE project CASCADE CONSTRAINTS ;

CREATE TABLE department
  (
    department_id INTEGER NOT NULL ,
    name          VARCHAR2 (256)
  ) ;
ALTER TABLE department ADD CONSTRAINT department_PK PRIMARY KEY ( department_id ) ;


CREATE TABLE employee
  (
    employee_id INTEGER NOT NULL ,
    name        VARCHAR2 (256) ,
    salary      INTEGER
  ) ;
ALTER TABLE employee ADD CONSTRAINT employee_PK PRIMARY KEY ( employee_id ) ;


CREATE TABLE je
  (
    je_id       INTEGER NOT NULL ,
    job_id      INTEGER NOT NULL ,
    employee_id INTEGER NOT NULL
  ) ;
ALTER TABLE je ADD CONSTRAINT je_PK PRIMARY KEY ( je_id ) ;


CREATE TABLE job
  ( job_id INTEGER NOT NULL , name VARCHAR2 (256)
  ) ;
ALTER TABLE job ADD CONSTRAINT job_PK PRIMARY KEY ( job_id ) ;


CREATE TABLE meeting
  (
    meeting_id INTEGER NOT NULL ,
    "date"     DATE ,
    room       INTEGER
  ) ;
ALTER TABLE meeting ADD CONSTRAINT meeting_PK PRIMARY KEY ( meeting_id ) ;


CREATE TABLE pem
  (
    pem_id      INTEGER NOT NULL ,
    employee_id INTEGER NOT NULL ,
    meeting_id  INTEGER NOT NULL ,
    project_id  INTEGER NOT NULL
  ) ;
ALTER TABLE pem ADD CONSTRAINT pem_PK PRIMARY KEY ( pem_id ) ;


CREATE TABLE project
  (
    project_id    INTEGER NOT NULL ,
    name          VARCHAR2 (256) ,
    department_id INTEGER NOT NULL ,
    manager       INTEGER NOT NULL
  ) ;
CREATE UNIQUE INDEX project__IDX ON project
  (
    manager ASC
  )
  ;
ALTER TABLE project ADD CONSTRAINT project_PK PRIMARY KEY ( project_id ) ;


ALTER TABLE je ADD CONSTRAINT r1 FOREIGN KEY ( job_id ) REFERENCES job ( job_id ) ;

ALTER TABLE project ADD CONSTRAINT r12 FOREIGN KEY ( manager ) REFERENCES employee ( employee_id ) ;

ALTER TABLE je ADD CONSTRAINT r2 FOREIGN KEY ( employee_id ) REFERENCES employee ( employee_id ) ;

ALTER TABLE project ADD CONSTRAINT r3 FOREIGN KEY ( department_id ) REFERENCES department ( department_id ) ;

ALTER TABLE pem ADD CONSTRAINT r6 FOREIGN KEY ( employee_id ) REFERENCES employee ( employee_id ) ;

ALTER TABLE pem ADD CONSTRAINT r7 FOREIGN KEY ( meeting_id ) REFERENCES meeting ( meeting_id ) ;

ALTER TABLE pem ADD CONSTRAINT r8 FOREIGN KEY ( project_id ) REFERENCES project ( project_id ) ;

CREATE SEQUENCE department_department_id_SEQ START WITH 1 NOCACHE ORDER ;
CREATE OR REPLACE TRIGGER department_department_id_TRG BEFORE
  INSERT ON department FOR EACH ROW WHEN (NEW.department_id IS NULL) BEGIN :NEW.department_id := department_department_id_SEQ.NEXTVAL;
END;
/

CREATE SEQUENCE employee_employee_id_SEQ START WITH 1 NOCACHE ORDER ;
CREATE OR REPLACE TRIGGER employee_employee_id_TRG BEFORE
  INSERT ON employee FOR EACH ROW WHEN (NEW.employee_id IS NULL) BEGIN :NEW.employee_id := employee_employee_id_SEQ.NEXTVAL;
END;
/

CREATE SEQUENCE je_je_id_SEQ START WITH 1 NOCACHE ORDER ;
CREATE OR REPLACE TRIGGER je_je_id_TRG BEFORE
  INSERT ON je FOR EACH ROW WHEN (NEW.je_id IS NULL) BEGIN :NEW.je_id := je_je_id_SEQ.NEXTVAL;
END;
/

CREATE SEQUENCE job_job_id_SEQ START WITH 1 NOCACHE ORDER ;
CREATE OR REPLACE TRIGGER job_job_id_TRG BEFORE
  INSERT ON job FOR EACH ROW WHEN (NEW.job_id IS NULL) BEGIN :NEW.job_id := job_job_id_SEQ.NEXTVAL;
END;
/

CREATE SEQUENCE meeting_meeting_id_SEQ START WITH 1 NOCACHE ORDER ;
CREATE OR REPLACE TRIGGER meeting_meeting_id_TRG BEFORE
  INSERT ON meeting FOR EACH ROW WHEN (NEW.meeting_id IS NULL) BEGIN :NEW.meeting_id := meeting_meeting_id_SEQ.NEXTVAL;
END;
/

CREATE SEQUENCE pem_pem_id_SEQ START WITH 1 NOCACHE ORDER ;
CREATE OR REPLACE TRIGGER pem_pem_id_TRG BEFORE
  INSERT ON pem FOR EACH ROW WHEN (NEW.pem_id IS NULL) BEGIN :NEW.pem_id := pem_pem_id_SEQ.NEXTVAL;
END;
/

CREATE SEQUENCE project_project_id_SEQ START WITH 1 NOCACHE ORDER ;
CREATE OR REPLACE TRIGGER project_project_id_TRG BEFORE
  INSERT ON project FOR EACH ROW WHEN (NEW.project_id IS NULL) BEGIN :NEW.project_id := project_project_id_SEQ.NEXTVAL;
END;
/


-- Oracle SQL Developer Data Modeler Summary Report: 
-- 
-- CREATE TABLE                             7
-- CREATE INDEX                             1
-- ALTER TABLE                             14
-- CREATE VIEW                              0
-- ALTER VIEW                               0
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           7
-- ALTER TRIGGER                            0
-- CREATE COLLECTION TYPE                   0
-- CREATE STRUCTURED TYPE                   0
-- CREATE STRUCTURED TYPE BODY              0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          7
-- CREATE MATERIALIZED VIEW                 0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              0
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- REDACTION POLICY                         0
-- 
-- ORDS DROP SCHEMA                         0
-- ORDS ENABLE SCHEMA                       0
-- ORDS ENABLE OBJECT                       0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0
